import React from 'react';
// import App from '../App'
import Contact from './Contact'
import Projects from './Projects'
import Home from './Home'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faGitlab, faLinkedinIn } from '@fortawesome/free-brands-svg-icons'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import '../App.scss';

class NavBar extends React.Component {
    makeNav() {
        let x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    render() {
        return (
            <Router basename={`${process.env.PUBLIC_URL}/`}>
                <div>
                    <div className="topnav" id="myTopnav">
                        <span className= "dev-name">Andrii Roizin</span>
                        <a href="https://gitlab.com/dashboard/projects" target="_blank"  rel="noopener noreferrer"><FontAwesomeIcon icon={faGitlab} className="git" /></a>
                        <a href="https://www.linkedin.com/in/andrii-roizin-aa736a199/" target="_blank"  rel="noopener noreferrer"><FontAwesomeIcon icon={faLinkedinIn}/></a>                        
                        <Link to="/contact" className="no-icon">Contact me</Link>
                        <Link to="/projects" className="no-icon">Projects</Link> 
                        <Link to="/" className="no-icon">Home</Link>

                        <span className="icon" onClick={this.makeNav}>
                            <FontAwesomeIcon icon={faBars} />
                        </span>
                    </div>

                    <Switch>
                        <Route path="/projects">
                            <Projects />
                        </Route>
                        <Route path="/contact">
                            <Contact />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>
            </Router>
        );

    }
}

export default NavBar;
