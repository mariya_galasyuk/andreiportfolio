import React from 'react';
import '../App.scss';
import Carousel from 'react-bootstrap/Carousel'
import Aos from 'aos'

class Projects extends React.Component {
  render() {
    Aos.init({
      duration: 1200,
    })
    return <div>
      <div className="container">
        <div className="projects-intro">
          <div className="name-animation" style={{ height: "100px" }}>
            <div className="name-block info-block">
              <div className="text-top">
                <div>
                  <span>Portfolio</span>
                </div>
              </div>
            </div>
          </div>
          <span className="projects-text"> During the last two years of my education at the university, I have been doing a lot of projects, especially in teams or pairs.
        I usually find myself in the position of product owner or the scrum master, so I can not only code, but also actively participate
        in the workflow planning. Here are some examples of them and I have a lot more smaller on my git as well.
        </span>
        </div>
        <div className="project" data-aos="fade-right">
          <div className="project-description-block">
            <div className="project-description">
              <h2>Coatchplaats</h2>
              <p>In this project in a team of 4, we developed the Android application for the company named Hishna located in Deventer. The idea of the app is that companies can find different types of coaches and hire them to improve their working processes. I added the git link so you can install the APK and try it yourself or have a look at the documentation with all the designs and more explanation in the docs folder in the repo.
The link is:<a href="https://gitlab.com/Andrii_Roizin/coachplaats"> https://gitlab.com/Andrii_Roizin/coachplaats</a>
              </p>
            </div>
          </div>
          <div className="slideshow-back">
            <div className="slideshow">
              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/coachplaats/0.PNG')}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/coachplaats/1.PNG')}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/coachplaats/2.PNG')}
                    alt="Third slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/coachplaats/3.PNG')}
                    alt="Third slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/coachplaats/4.PNG')}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>

            </div>
          </div>
        </div>

        <div className="project" data-aos="fade-left">
          <div className="project-description-block">
            <div className="project-description">
              <h2>FBS Project</h2>
              <p>In a team, we developed an application for the Facility Business School student organization of Saxion University.
              They needed this app to be supported by both IOS and Android so we used flutter to accomplish this requirement.
              The idea of the application that they can publish their events and send invitations to all users. It means that every student
              who takes part in this association can see all the events and come there only with his account in this app( they required
              ID check to enter the event before). Unfortunately, I can not provide access to the source code because they will use it.
              I posted a link to the Git repository with only documentation in there so you can have a look at our Functional design and System file.
              The link is:<a href="https://gitlab.com/Andrii_Roizin/fbs-project-documentation/-/tree/master/">https://gitlab.com/Andrii_Roizin/fbs-project-documentation/-/tree/master/</a>
              </p>
            </div>
          </div>
          <div className="slideshow-back">
            <div className="slideshow">
              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/fbs/screen1.png')}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/fbs/screen2.png')}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/fbs/screen3.png')}
                    alt="Third slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/fbs/screen4.png')}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>

            </div>
          </div>
        </div>

        <div className="project" data-aos="fade-right">
          <div className="project-description-block">
            <div className="project-description">
              <h2>Compilers&OS</h2>
              <p>For this project, I together with a fellow student are developing our programming language based on Java Virtual Machine.
              It allowed me to learn Java much deeper than I knew before and experiment with bytecode. At the pictures you can see an example
                of our program, java class and bytecode generated from it. The project link is: <a href="https://gitlab.com/saxion.nl/2.3-compilers-and-os/64/-/tree/testo">https://gitlab.com/saxion.nl/2.3-compilers-and-os/64/-/tree/testo</a>.
                A documentation with the explanation can be found in the docs folder.
                  </p>
            </div>
          </div>
          <div className="slideshow-back">
            <div className="slideshow">
              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/compilers/screen1.PNG')}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/compilers/screen2.PNG')}
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block img-fluid"
                    src={require('../img/compilers/screen3.PNG')}
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>

            </div>
          </div>
        </div>
      </div>
    </div>
  }

}

export default Projects;


