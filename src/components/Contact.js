import React from 'react';
import '../App.scss';

class Contact extends React.Component {
  render() {
    return <div>
      <div className="contacts-block" data-aos="fade-up">
      <p style={{"textAlign":"center"}}><a href="CV.pdf" download="AndriiRoizinCV" target="_blank" type="application/octet-stream" rel="noopener noreferrer">Download CV</a>
        </p>
        <div>
        </div>
        <p><b>Address: </b>Brandts Buyspark 54, 7425GD, Deventer, The Netherlands</p>
        <p><b>Tel:</b> +31 6 44340349</p>
        <p><b>Email:</b> <a href="mailto: andreyroizin14@gmail.com" target="_blank"  rel="noopener noreferrer">andreyroizin14@gmail.com</a>,  <a href="mailto: 465182@student.saxion.nl" target="_blank"  rel="noopener noreferrer">465182@student.saxion.nl</a></p>

      </div>
    </div>
  }
}

export default Contact;
