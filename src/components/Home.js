import React from 'react';
import '../App.scss';
import Aos from 'aos'
class Home extends React.Component {
  render() {
    Aos.init({
      duration: 1200,
    })
    return <div>
      <div className="container">
        <div className="color-block">
          <div className="image-name">
            <div className="image-block">
              <div className="portfolio-image">
                {/* <img width="50px" src="../../public/img/image.jpg"></img> */}
              </div>
            </div>
            <div className="animation-block">
              <div className="name-animation">
                <div className="name-block">
                  <div className="text-top">
                    <div>
                      <span>Andrii Roizin</span>
                    </div>
                  </div>
                  <div className="text-bottom">
                    <div>software engineer</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="description-block"  data-aos="fade-up">
          <div className="name-animation" style={{ height: "100px" }}>
            <div className="name-block info-block">
              <div className="text-top">
                <div>
                  <span>ABOUT ME</span>
                </div>
              </div>
            </div>
          </div>
          <div className="columns-block">
            <div className="description" data-aos="fade-right">
              A versatile and professional software engineering student
              of the second year at the Saxion University of Applied Science in The Netherlands, studying in English.
              My native country is Ukraine. I am actively looking for a full-time internship for next year, as a requirement
              of my study program. The preferable period is the 1st of September 2020  –  the 1st of February 2021.
              
              <p></p>
              
              With 3 years programming background I have worked with rather big variety of languages and techniques listed on the right.
              I am looking for a chance to make use of knowledge I have obtained during these years as well as personal and professional growth.
              Since I have worked in a number of group projects, I have a strong experience in teamwork, interacting with people 
              in general and being scrum master in particular. Group members have described me as hard-working, highly motivated and 
              responsible.
          </div>
            <div className="skills-block" data-aos="fade-left">
              <ul>
              <li>	Java and Android development</li>
              <li>	Algorithmisation and Data structures</li>
              <li>	C++</li>
              <li>	Python</li>
              <li>	Testing</li>
              <li>	Scrum methodology</li>
              <li>	OOP principles</li>
              <li>	GIT</li>
              <li>	Software design</li>
              <li>	Javascript</li>
              <li>	HTML, CSS</li>
              <li>	SQL</li>
              <li>	Flutter</li>
              </ul>

            </div>
          </div>

        </div>
      </div>
    </div>
  }

}

export default Home;
